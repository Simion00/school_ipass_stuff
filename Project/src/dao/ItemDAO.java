package dao;

import java.util.List;

import model.Item;

public interface ItemDAO {
	public void addItem(Item item);

	public void deleteItem(int itemId);

	public void updateItem(Item item);

	public void setItemPriority(int itemId, boolean priority);

	public void setItemActive(int itemId, boolean active);

	public List<Item> getAllItems(String...sort);

	public Item getItemById(int itemId);
}