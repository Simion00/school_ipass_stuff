package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import model.Item;
import util.DBUtil;

public class ItemDAOImplementation implements ItemDAO {

	private Connection conn;

	public ItemDAOImplementation() {
		conn = DBUtil.getConnection();
	}

	@Override
	public void addItem(Item item) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date1 = new Date();
			try {
				date1 = sdf.parse(item.getEnd_date());
			} catch (ParseException e) {
				e.printStackTrace();
			}
            Date date2 = new Date();
			if(date1.after(date2)){
				String query = "insert into item (ID, active, priority, end_date, category, description) values (?,?,?,?,?,?)";
				PreparedStatement preparedStatement = conn.prepareStatement(query);
				preparedStatement.setInt(1, item.getItem_ID());
				preparedStatement.setBoolean(2, item.isActive());
				preparedStatement.setBoolean(3, item.isPriority());
				preparedStatement.setString(4, item.getEnd_date());
				preparedStatement.setString(5, item.getCategory());
				preparedStatement.setString(6, item.getDescription());
				preparedStatement.executeUpdate();
				preparedStatement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteItem(int itemId) {
		try {
			String query = "delete from item where ID=?";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setInt(1, itemId);
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateItem(Item item) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date1 = new Date();
			try {
				date1 = sdf.parse(item.getEnd_date());
			} catch (ParseException e) {
				e.printStackTrace();
			}
            Date date2 = new Date();
			if(date1.after(date2)){
				String query = "update item set active=?, priority=?, end_date=?, category=?, description=? where ID=?";
				PreparedStatement preparedStatement = conn.prepareStatement(query);
				preparedStatement.setBoolean(1, item.isActive());
				preparedStatement.setBoolean(2, item.isPriority());
				preparedStatement.setString(3, item.getEnd_date());
				preparedStatement.setString(4, item.getCategory());
				preparedStatement.setString(5, item.getDescription());
				preparedStatement.setInt(6, item.getItem_ID());
				preparedStatement.executeUpdate();
				preparedStatement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setItemPriority(int itemId, boolean priority) {
		try {
			String query = "update item set priority=? where ID=?";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setBoolean(1, priority);
			preparedStatement.setInt(2, itemId);
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setItemActive(int itemId, boolean active) {
		try {
			String query = "update item set active=? where ID=?";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setBoolean(1, active);
			preparedStatement.setInt(2, itemId);
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Item> getAllItems(String...sort) {
		String sortBy = sort.length == 1 && sort[0] != null ? sort[0] : "category";
		List<Item> items = new ArrayList<Item>();
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from item");
			while (resultSet.next()) {
				Item item = new Item();
				item.setItem_ID(resultSet.getInt("ID"));
				item.setActive(resultSet.getBoolean("active"));
				item.setPriority(resultSet.getBoolean("priority"));
				item.setEnd_date(resultSet.getString("end_date"));
				item.setCategory(resultSet.getString("category"));
				item.setDescription(resultSet.getString("description"));
				items.add(item);
			}
			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(sortBy.equals("category")){
			return items.stream().sorted((object1, object2) -> object1.getCategory().compareTo(object2.getCategory())).collect(Collectors.toList());
		}else if(sortBy.equals("priority")){
			return items.stream().sorted((object1, object2) -> Boolean.compare(object2.isPriority(), object1.isPriority())).collect(Collectors.toList());
		}else if(sortBy.equals("end_date")){
			return items.stream().sorted((object1, object2) -> object1.getEnd_date().compareTo(object2.getEnd_date())).collect(Collectors.toList());
		}else if(sortBy.equals("sort_ID")){
			return items;
		}else{
			return items.stream().sorted((object1, object2) -> object1.getCategory().compareTo(object2.getCategory())).collect(Collectors.toList());
		}
	}

	@Override
	public Item getItemById(int itemId) {
		Item item = new Item();
		try {
			String query = "select * from item where ID=?";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setInt(1, itemId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				item.setItem_ID(resultSet.getInt("ID"));
				item.setActive(resultSet.getBoolean("active"));
				item.setPriority(resultSet.getBoolean("priority"));
				item.setEnd_date(resultSet.getString("end_date"));
				item.setCategory(resultSet.getString("category"));
				item.setDescription(resultSet.getString("description"));
			}
			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return item;
	}

}