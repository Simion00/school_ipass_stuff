package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ItemDAO;
import dao.ItemDAOImplementation;
import model.Item;

@WebServlet("/ItemController")
public class ItemController extends HttpServlet {

	private ItemDAO dao;
	private static final long serialVersionUID = 1L;
	public static final String lIST_ITEM = "/listItem.jsp";
	public static final String INSERT_OR_EDIT = "/item.jsp";

	public ItemController() {
		dao = new ItemDAOImplementation();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String forward = "";
		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("delete")) {
			forward = lIST_ITEM;
			int item_ID = Integer.parseInt(request.getParameter("item_ID"));
			dao.deleteItem(item_ID);
			request.setAttribute("items", dao.getAllItems());
			request.setAttribute("active", true);
			request.setAttribute("sorted", "category");
		} else if (action.equalsIgnoreCase("sortBy")) {
			forward = lIST_ITEM;
			String sortBy = request.getParameter("sortBy");
			request.setAttribute("items", dao.getAllItems(sortBy));
			request.setAttribute("active", sortBy.equals("inactive") ? false : true);
			request.setAttribute("sorted", sortBy.equals("active") || sortBy.equals("inactive") ? "category" : sortBy);
		} else if (action.equalsIgnoreCase("active")) {
			forward = lIST_ITEM;
			int item_ID = Integer.parseInt(request.getParameter("item_ID"));
			boolean active = Boolean.parseBoolean(request.getParameter("active"));
			dao.setItemActive(item_ID, !active);
			request.setAttribute("items", dao.getAllItems());
			request.setAttribute("active", true);
			request.setAttribute("sorted", "category");
		} else if (action.equalsIgnoreCase("priority")) {
			forward = lIST_ITEM;
			int item_ID = Integer.parseInt(request.getParameter("item_ID"));
			boolean priority = Boolean.parseBoolean(request.getParameter("priority"));
			dao.setItemPriority(item_ID, !priority);
			request.setAttribute("items", dao.getAllItems());
			request.setAttribute("active", true);
			request.setAttribute("sorted", "category");
		} else if (action.equalsIgnoreCase("edit")) {
			forward = INSERT_OR_EDIT;
			int item_ID = Integer.parseInt(request.getParameter("item_ID"));
			Item item = dao.getItemById(item_ID);
			request.setAttribute("item", item);
		} else if (action.equalsIgnoreCase("insert")) {
			forward = INSERT_OR_EDIT;
		} else {
			forward = lIST_ITEM;
			request.setAttribute("items", dao.getAllItems());
			request.setAttribute("active", true);
			request.setAttribute("sorted", "category");
		}
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Item item = new Item();
		item.setActive(Boolean.parseBoolean(request.getParameter("active")));
		item.setPriority(Boolean.parseBoolean(request.getParameter("priority")));
		item.setEnd_date(request.getParameter("end_date"));
		item.setCategory(request.getParameter("category"));
		item.setDescription(request.getParameter("description"));
		String item_ID = request.getParameter("item_ID");

		if (item_ID == null || item_ID.isEmpty())
			dao.addItem(item);
		else {
			item.setItem_ID(Integer.parseInt(item_ID));
			dao.updateItem(item);
		}
		RequestDispatcher view = request.getRequestDispatcher(lIST_ITEM);
		request.setAttribute("items", dao.getAllItems());
		request.setAttribute("active", true);
		request.setAttribute("sorted", "category");
		view.forward(request, response);
	}
}