package model;

public class Item {

	private int item_ID;
	private boolean active;
	private boolean priority;
	private String end_date;
	private String category;
	private String description;

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isPriority() {
		return priority;
	}

	public void setPriority(boolean priority) {
		this.priority = priority;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getItem_ID() {
		return item_ID;
	}

	public void setItem_ID(int item_ID) {
		this.item_ID = item_ID;
	}

	@Override
	public String toString() {
		return "Item [item_ID=" + item_ID + ", active=" + active + ", priority=" + priority + ", end_date=" + end_date
				+ ", category=" + category + ", description=" + description + "]";
	}
}