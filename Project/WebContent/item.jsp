<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Add New Item</title>
</head>
<body>
	<form action="ItemController.do" method="post">
		<fieldset>
			<div>
				<label for="item_ID">Item ID</label> <input type="text"
					name="item_ID" value="<c:out value="${item.item_ID}" />"
					readonly="readonly" placeholder="Item ID" />
			</div>
			<div>
				<label for="active">Active</label> <input type="text"
					name="active" value="<c:out value="${item.active}" />"
					placeholder="Active" />
			</div>
			<div>
				<label for="priority">Priority</label> <input type="text"
					name="priority" value="<c:out value="${item.priority}" />"
					placeholder="Priority" />
			</div>
			<div>
				<label for="end_date">End date</label> <input type="text" name="end_date"
					value="<c:out value="${item.end_date}" />" placeholder="End date" />
			</div>
			<div>
				<label for="category">Category</label> <input type="text" name="category"
					value="<c:out value="${item.category}" />" placeholder="Category" />
			</div>
			<div>
				<label for="description">Description</label> <input type="text" name="description"
					value="<c:out value="${item.description}" />" placeholder="Description" />
			</div>
			<div>
				<input type="submit" value="Submit" />
			</div>
		</fieldset>
	</form>
</body>
</html>