<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Show All Items</title>
</head>
<body>
	<c:set var="category" value="null" />
	<c:set var="sort_ID" value="0" />
	<c:set var="priority" value="null" />
	<c:set var="end_date" value="null" />
	<div>
		<a href="ItemController.do?action=sortBy&sortBy=category">Category</a><br>
		<a href="ItemController.do?action=sortBy&sortBy=sort_ID">Item ID</a><br>
		<a href="ItemController.do?action=sortBy&sortBy=priority">Priority</a><br>
		<a href="ItemController.do?action=sortBy&sortBy=end_date">End date</a><br>
		<c:if test="${active == 'true'}">
			<a href="ItemController.do?action=sortBy&sortBy=inactive">Inactive</a>
			<br>
		</c:if>
		<c:if test="${active == 'false'}">
			<a href="ItemController.do?action=sortBy&sortBy=active">Active</a>
			<br>
		</c:if>
	</div>
	<table>
		<c:forEach items="${items}" var="item">
			<c:if test="${item.active == active}">
				<c:set var="ifTitle" value="false" />
				<c:if test="${sorted == 'category'}">
					<c:if test="${item.category != category}">
						<c:set var="ifTitle" value="true" />
					</c:if>
				</c:if>
				<c:if test="${sorted == 'sort_ID'}">
					<c:if test="${item.item_ID != sort_ID}">
						<c:set var="ifTitle" value="true" />
					</c:if>
				</c:if>
				<c:if test="${sorted == 'priority'}">
					<c:if test="${item.priority != priority}">
						<c:set var="ifTitle" value="true" />
					</c:if>
				</c:if>
				<c:if test="${sorted == 'end_date'}">
					<c:if test="${item.end_date != end_date}">
						<c:set var="ifTitle" value="true" />
					</c:if>
				</c:if>
				<c:if test="${ifTitle == 'true'}">
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<c:if test="${sorted == 'category'}">
								<c:out value="${item.category}" />
							</c:if>
							<c:if test="${sorted == 'sort_ID'}">
								<c:out value="${item.item_ID}" />
							</c:if>
							<c:if test="${sorted == 'priority'}">
								<c:out value="${item.priority}" />
							</c:if>
							<c:if test="${sorted == 'end_date'}">
								<c:out value="${item.end_date}" />
							</c:if>
						</td>
					</tr>
					<tr>
						<th>Item ID</th>
						<th>Active</th>
						<th>Priority</th>
						<th>End date</th>
						<th>Category</th>
						<th>Description</th>
						<th colspan="2">Action</th>
					</tr>
				</c:if>
				<c:set var="category" value="${item.category}" />
				<c:set var="sort_ID" value="${item.item_ID}" />
				<c:set var="priority" value="${item.priority}" />
				<c:set var="end_date" value="${item.end_date}" />
				<tr>
					<td><c:out value="${item.item_ID}" /></td>
					<td><a
						href="ItemController.do?action=active&item_ID=<c:out value="${item.item_ID}"/>&active=<c:out value="${item.active}"/>"><c:out
								value="${item.active}" /></a></td>
					<td><a
						href="ItemController.do?action=priority&item_ID=<c:out value="${item.item_ID}"/>&priority=<c:out value="${item.priority}"/>"><c:out
								value="${item.priority}" /></a></td>
					<td><c:out value="${item.end_date}" /></td>
					<td><c:out value="${item.category}" /></td>
					<td><c:out value="${item.description}" /></td>
					<c:if test="${active == 'true'}">
						<td><a
							href="ItemController.do?action=edit&item_ID=<c:out value="${item.item_ID}"/>">Update</a></td>
					</c:if>
					<c:if test="${active == 'false'}">
						<td><a
							href="ItemController.do?action=delete&item_ID=<c:out value="${item.item_ID}"/>">Delete</a></td>
					</c:if>
				</tr>
			</c:if>
		</c:forEach>
	</table>
	<p>
		<a href="ItemController.do?action=insert">Add Item</a>
	</p>
</body>
</html>